<?php
class Structure extends CI_Model {

    public function getDataForm(){
        $data[0]['context'] = $_POST['quemsomos'];
        $data[1]['context'] = $this->input->post('email');
        $data[2]['context'] = $this->input->post('telefone');
        $data[3]['context'] = $this->input->post('endereco');
        $data[4]['context'] = $this->input->post('linkgoogle');
        return $data;
    }

    public function updateContent(){
        $data = $this->getDataForm();
        for($i = 0; $i < 5; $i++){
            $this->db->where('id', $i+1);
            $this->db->update('content', $data[$i]);
        }
        
    }

    public function selectAll(){
        return $this->db->get('content')->result_array();
    }

}