<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CarrosselController extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Carrossel', 'model');
    }

	public function index(){
        $html = $this->load->view('pages/adm/imagesedit', null, true);
        $this->show($html);
    }

    public function do_upload(){
        $this->model->update();
        return redirect(base_url());    
    }
    
}