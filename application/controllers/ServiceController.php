<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceController extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Service', 'model');
    }

	public function index(){
        $contato['action'] = base_url('servicecontroller/store');
        $html = $this->load->view('pages/service/index', $contato, true);
        $this->show($html);
    }

    public function store(){
        $this->model->store();
        redirect(base_url('servicecontroller/list'));
    }

    public function list(){
        $contatos['lista'] = $this->model->selectAll();
        $html = $this->load->view('pages/service/list', $contatos, true);
        $this->show($html);
    }

    public function edit($id){
        $contato = $this->model->select($id);
        $contato['action'] = base_url('servicecontroller/update/'.$id);
        $html = $this->load->view('pages/service/index', $contato, true);
        $this->show($html);
    }

    public function update($id){
        $this->model->update($id);
        redirect(base_url('servicecontroller/list'));
    }

    public function destroy($id){
        $this->model->destroy($id);
        redirect(base_url('servicecontroller/list'));
    }
    
}