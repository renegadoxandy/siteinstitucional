<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Structure', 'model');
    }

	public function index()
	{
        $this->load->model('Service', 'service');
        $this->load->model('Carrossel', 'carrossel');
        $data['structure']['content'] = $this->model->selectAll();
        $data['service']['services'] = $this->service->selectAll();
        $data['carrossel']['urlimg'] = $this->carrossel->selectAll();
        $html = $this->load->view('includes/carrossel', $data['carrossel'], true);
        $html .= $this->load->view('pages/main/quemSomos', $data['structure'], true);
        $html .= $this->load->view('pages/main/servicos', $data['service'], true);
        $html .= $this->load->view('pages/main/contato', $data['structure'], true);
        $this->show($html);
    }
    
    public function administration(){
        $data['content'] = $this->model->selectAll();
        $html = $this->load->view('pages/adm/form', $data, true);
        $this->show($html);
    }

    public function updateStructure(){
        $this->model->updateContent();
        return redirect(base_url());
    }
}