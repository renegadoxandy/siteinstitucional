<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class MY_Controller  extends CI_Controller {

    function __construct() {
		parent::__construct ();
	}

	protected function show($content) {
		$html  = $this->load->view('basic/header', null, true);
		$html .= $this->load->view('includes/navbar', null, true);
    $html .= $content;
    $html .= $this->load->view('basic/footer', null, true);
    echo $html;
	}
}
