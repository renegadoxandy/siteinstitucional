<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="<?= base_url('#')?>">NET</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('#quemSomos')?>">Quem Somos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('#servicos')?>">Serviços</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('#contato')?>">Contato</a>
      </li>
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item">
        <div class="dropdown">
          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-right:100px;">
            Editar
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="<?=base_url('/homecontroller/administration')?>">Estrutura Base</a>
            <a class="dropdown-item" href="<?=base_url('/servicecontroller/index')?>">Criar Serviço</a>
            <a class="dropdown-item" href="<?=base_url('/servicecontroller/list')?>">Editar Serviços</a>
            <a class="dropdown-item" href="<?=base_url('/carrosselcontroller/index')?>">Imagens</a>
          </div>
        </div>
      </li>
    </ul>
  </div>
</nav>