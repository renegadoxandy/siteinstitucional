<section class="jumbotron" style="margin:0px;" id="quemSomos">
    <div class="container">
        <h1 class="jumbotron-heading">Informações da página</h1><br>
        <form action="<?= base_url('/homecontroller/updateStructure')?>" method="POST">
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Quem Somos</label>
                <textarea name="quemsomos" class="form-control" id="exampleFormControlTextarea1" rows="3"><?= $content[0]['context'] ?></textarea>
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput1">Email</label>
                <input name="email" type="email" class="form-control" id="formGroupExampleInput1" value="<?= $content[1]['context'] ?>">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Telefone</label>
                <input name="telefone" type="text" class="form-control" id="formGroupExampleInput2" value="<?= $content[2]['context'] ?>">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput3">Endereço</label>
                <input name="endereco" type="text" class="form-control" id="formGroupExampleInput3" value="<?= $content[3]['context'] ?>">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput4">Link mapa do Google</label>
                <input name="linkgoogle" type="text" class="form-control" id="formGroupExampleInput4" value="<?= $content[4]['context'] ?>">
            </div>
            <button type="submit" class="btn btn-primary">Salvar</button>
        </form>
    </div>
</section>