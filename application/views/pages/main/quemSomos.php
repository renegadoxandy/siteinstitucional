<section class="jumbotron text-center" style="margin:0px;" id="quemSomos">
    <div class="container" >
        <div class="row">
            <div class="col-md-12">
                <h1 class="jumbotron-heading">Quem Somos</h1><br>
                <span class="lead text-muted"><?= $content[0]['context'] ?></span>
            </div>
        </div>
        
    </div>
</section>
