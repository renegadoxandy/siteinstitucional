<section class="jumbotron text-center" style="margin:0px; background:#ccc;" id="contato">
    <div class="container">
        <h1 class="jumbotron-heading">Contato</h1><br>
        <p class="lead text-muted"><strong>Email: </strong><?= $content[1]['context'] ?></p>
        <p class="lead text-muted"><strong>Telefone: </strong><?= $content[2]['context'] ?></p>
        <p class="lead text-muted"><strong>Endereço: </strong><?= $content[3]['context'] ?></p>
        <iframe src="<?= $content[4]['context'] ?>" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</section>