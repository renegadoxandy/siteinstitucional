<section class="jumbotron text-center" style="margin:0px; background:#ddd;" id="servicos">
    <div class="container">
        <h1 class="jumbotron-heading mb-5">Serviços</h1><br>
        <div class="row">
            <?php foreach ($services as $service): ?>
            <div class="col-sm-6">
                <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><?=$service['title']?></h5>
                    <p class="card-text"><?=$service['text']?></p>
                    <span class="btn btn-primary">R$<?=$service['price']?>,00</span>
                </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>