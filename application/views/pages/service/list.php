<section class="jumbotron" style="min-height:650px;margin:0px">
<div class="container mt-3">
    <ul>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Título</th>
                <th scope="col">Preço</th>
                <th scope="col" width="300px">Ações</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($lista as $item): ?>
                <tr>
                    <th scope="row"><?=$item['id']?></th>
                    <td><?=$item['title']?></td>
                    <td><?=$item['price']?></td>
                    <td>
                        <a href="<?= base_url('servicecontroller/edit/'.$item['id']) ?>"><button type="button" class="btn btn-primary">Editar</button></a>
                        <a href="<?= base_url('servicecontroller/destroy/'.$item['id']) ?>"><button type="button" class="btn btn-danger">Deletar</button></a>
                    </td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>

    </ul>
</div>
</section>