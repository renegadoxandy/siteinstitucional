-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 07-Mar-2019 às 04:23
-- Versão do servidor: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbsiteinstitucional`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `carrossel`
--

CREATE TABLE `carrossel` (
  `id` int(11) NOT NULL,
  `url` varchar(200) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `carrossel`
--

INSERT INTO `carrossel` (`id`, `url`, `update_at`) VALUES
(1, 'mix_Parallax_Desk.jpg', '2019-03-07 03:16:08'),
(2, 'nasce_estrela_Parallax_Desk.jpg', '2019-03-07 03:16:08'),
(3, 'twd_Parallax_Desk.jpg', '2019-03-07 03:16:08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `content`
--

CREATE TABLE `content` (
  `id` int(11) NOT NULL,
  `context` varchar(1000) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `content`
--

INSERT INTO `content` (`id`, `context`, `update_at`) VALUES
(1, 'NET é uma empresa de telecomunicações brasileira que oferece serviços como televisão por assinatura, acesso à internet e telefonia fixa. A Embratel possui 96,16% da empresa, que tem como empresário o bilionário Carlos Slim. 3,84%, pertencem à EGPar, uma sociedade entre o Grupo Globo e a própria Embratel. A divisão societária da EGPar corresponde a 49,55% da Globo e 50,45% da Embratel.', '2019-03-07 03:13:57'),
(2, 'contato@net.com', '2019-03-07 03:18:10'),
(3, '0800-7254114', '2019-03-07 03:18:10'),
(4, 'Av. Salgado Filho, 3501 - Centro, Guarulhos - SP, 07115-000', '2019-03-07 03:18:10'),
(5, 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14642.356876916925!2d-46.5374154!3d-23.4392016!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1f8cf929993dc97!2sInstituto+Federal+de+Educa%C3%A7%C3%A3o%2C+Ci%C3%AAncia+e+Tecnologia+de+S%C3%A3o+Paulo!5e0!3m2!1spt-BR!2sbr!4v1551642383602', '2019-03-04 23:21:44');

-- --------------------------------------------------------

--
-- Estrutura da tabela `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `text` text NOT NULL,
  `price` float NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `service`
--

INSERT INTO `service` (`id`, `title`, `text`, `price`, `update_at`) VALUES
(3, '240 Mega + TOP HD + Fone ilimitado', 'Dobro da velocidade: 120 Mega + 120 Mega por 6 meses! Ultravelocidade para ver vídeos, jogar online, ouvir músicas e navegar muito, ideal para 4 a 7 dispositivos conectados. Wi-Fi grátis dentro e fora de casa. Plano TOP HD com experiência completa de programação.', 258, '2019-03-07 03:19:47'),
(4, '35 Mega + MIX HD', 'Nossa dupla mais vendida Navegue com velocidade e estabilidade de conexão com Wi-Fi dentro e fora de casa. NET HDTV no plano MIX para curtir filmes, séries, esportes, infantil e em todas as telas com o NOW, onde e quando quiser.', 188, '2019-03-07 03:12:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carrossel`
--
ALTER TABLE `carrossel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carrossel`
--
ALTER TABLE `carrossel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
